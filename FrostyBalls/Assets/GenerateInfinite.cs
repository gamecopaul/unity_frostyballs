﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateInfinite : MonoBehaviour {

	float PlaneSize = 10.0f;
	int Distance = 5;
	int Location = 0;
	public GameObject PlanePrefab;
	public GameObject Ball;

	List<GameObject> GroundObjects;

	// Use this for initialization
	void Start () {
		GroundObjects = new List<GameObject>();
		for (int i = 0; i < Distance; i++)
		{
			CreateNewPlane(i);
		}
	}
	
	// Update is called once per frame
	void Update () {
		int current_location = (int)(Ball.transform.position.z / PlaneSize);
		while (current_location > Location)
		{
			CreateNewPlane(Location + Distance);
			Location++;
		}

		for (int i = GroundObjects.Count-1; i > 0; i--)
		{
			if (GroundObjects[i].transform.position.z < (Ball.transform.position.z - (PlaneSize * 2)))
			{
				GameObject temp = GroundObjects[i];
				GroundObjects.RemoveAt(i);
				Destroy(temp);
				break;
			}
		}
	}

	void CreateNewPlane(int location)
	{
		Vector3 pos = new Vector3(0, 0, PlaneSize * location);
		GameObject p = (GameObject)Instantiate(PlanePrefab, pos, Quaternion.identity);
		GroundObjects.Add(p);
	}
}
