﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateTerrain : MonoBehaviour {
	public float HeightScale = 5.0f;
	public float DetailScale = 5.0f;

	// Use this for initialization
	void Start () {
		Mesh mesh = this.GetComponent<MeshFilter>().mesh;
		Vector3[] vertices = mesh.vertices;
		for (int i = 0; i < vertices.Length; i++)
		{
			vertices[i].y = HeightScale * Mathf.PerlinNoise(	(vertices[i].x + this.transform.position.x) / DetailScale,
																(vertices[i].z + this.transform.position.z) / DetailScale);
		}
		mesh.vertices = vertices;
		mesh.RecalculateBounds();
		mesh.RecalculateNormals();
		//this.GetComponent<MeshCollider>().sharedMesh.vertices = vertices;
		//this.GetComponent<MeshCollider>().sharedMesh.RecalculateBounds();
		//this.GetComponent<MeshCollider>().sharedMesh.RecalculateNormals();

		this.gameObject.AddComponent<MeshCollider>();
	}

}
