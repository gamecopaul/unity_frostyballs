﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {

	public FrostyController FC;
	public FrostyController FC2;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey(KeyCode.UpArrow))
		{
			FC.FrostyInput.z = 1.0f;
		}
		else if (Input.GetKey(KeyCode.DownArrow))
		{
			FC.FrostyInput.z = -1.0f;
		}

		if (Input.GetKey(KeyCode.RightArrow))
		{
			FC.FrostyInput.x = 1.0f;
		}
		else if (Input.GetKey(KeyCode.LeftArrow))
		{
			FC.FrostyInput.x = -1.0f;
		}

		if (Input.GetKeyDown(KeyCode.Keypad0))
		{
			FC.FrostyInput.y = 1.0f;
		}


		if (Input.GetKey(KeyCode.W))
		{
			FC2.FrostyInput.z = 1.0f;
		}
		else if (Input.GetKey(KeyCode.S))
		{
			FC2.FrostyInput.z = -1.0f;
		}

		if (Input.GetKey(KeyCode.D))
		{
			FC2.FrostyInput.x = 1.0f;
		}
		else if (Input.GetKey(KeyCode.A))
		{
			FC2.FrostyInput.x = -1.0f;
		}

		if (Input.GetKeyDown(KeyCode.Space))
		{
			FC2.FrostyInput.y = 1.0f;
		}

	}
}
