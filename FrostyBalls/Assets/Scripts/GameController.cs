﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public GameObject Frosty1;
	public GameObject Frosty2;
	public float FallOffDistance = 0.4f;

	public Text FrostyText1;
	public Text FrostyText2;

	int FrostyScore1;
	int FrostyScore2;

	bool InMenus;
	public GameObject MainMenu;

	// Use this for initialization
	void Start () {
		FrostyScore1 = 0;
		FrostyScore2 = 0;
		GotoMainMenu();
	}
	
	// Update is called once per frame
	void Update () {

		if (!InMenus)
		{
			if (Frosty1.transform.position.y < FallOffDistance)
			{
				FrostyScore2 += 1;
				GotoMainMenu();
			}
			else if (Frosty2.transform.position.y < FallOffDistance)
			{
				FrostyScore1 += 1;
				GotoMainMenu();
			}
		}

		FrostyText1.text = FrostyScore1.ToString();
		FrostyText2.text = FrostyScore2.ToString();
	}

	void GotoMainMenu()
	{
		MainMenu.SetActive(true);
		InMenus = true;
		Frosty1.GetComponent<FrostyController>().InGame = false; 
		Frosty2.GetComponent<FrostyController>().InGame = false;
	}

	public void GotoGame()
	{
		MainMenu.SetActive(false);
		Frosty1.GetComponent<FrostyController>().Init(true);
		Frosty2.GetComponent<FrostyController>().Init(true);
		InMenus = false;
	}
}
