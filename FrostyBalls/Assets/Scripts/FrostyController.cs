﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrostyController : MonoBehaviour {

	public Vector3 FrostyInput;

	public float TopSize = 0.33f;
	public float MiddleSize = 0.66f;
	public float BottomSize = 1.0f;
	public float BallPenetration = 0.10f;
	public Vector3 StartPoint;

	Vector3 TopOffset = Vector3.zero;
	Vector3 MiddleOffset = Vector3.zero;

	public float Speed;
	public float Jump;
	Vector3 Velocity;

	public GameObject TopBall;
	public GameObject MiddleBall;
	public GameObject BottomBall;

	SphereCollider FrostyBaseCollider;
	Rigidbody FrostyRigidBody;

	public bool InGame;

	// Use this for initialization
	void Start() {
		FrostyBaseCollider = BottomBall.GetComponent<SphereCollider>();
		FrostyRigidBody = this.GetComponent<Rigidbody>();
		Init(false);
	}

	public void Init(bool inGame) {
		InGame = inGame;
		this.transform.position = StartPoint;
		MiddleOffset = new Vector3(0, (BottomSize / 2.0f) + ((MiddleSize/2.0f) * (1.0f -BallPenetration)), 0);
		TopOffset = new Vector3(0, (MiddleSize / 2.0f) + ((TopSize / 2.0f) * (1.0f - BallPenetration)), 0);

		BottomBall.transform.localScale = BottomSize * Vector3.one;
		MiddleBall.transform.localScale = MiddleSize * Vector3.one;
		TopBall.transform.localScale = TopSize * Vector3.one;

		FrostyRigidBody.isKinematic = true;
		FrostyRigidBody.angularVelocity = Vector3.zero;
		FrostyRigidBody.velocity = Vector3.zero;
	}

	// Update is called once per frame
	void FixedUpdate () {
		MiddleBall.transform.position = BottomBall.transform.position + MiddleOffset;
		TopBall.transform.position = MiddleBall.transform.position + TopOffset;

		Velocity = Speed * FrostyInput;

		if (this.transform.position.y <= 0.501f )
		{
			Velocity.y = Jump * FrostyInput.y;
		}
		else {
			Velocity.y = 0;
		}

		if (InGame)
		{
			if (FrostyRigidBody.isKinematic == true) FrostyRigidBody.isKinematic = false;
			FrostyRigidBody.AddForce(Velocity, ForceMode.Force);
		}

		FrostyInput = Vector3.zero;
	}
}
